package com.bigcommerce.testing.framework.testPlan;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.testng.TestNG;
import org.testng.annotations.Listeners;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import atu.testng.reports.listeners.ATUReportsListener;
import atu.testng.reports.listeners.ConfigurationListener;
import atu.testng.reports.listeners.MethodListener;


/*@Listeners({ ATUReportsListener.class, ConfigurationListener.class,
	  MethodListener.class })*/
public class DynamicTestRunner {
	
	
	//this one is working. Note: you do not have to use the static key work for static bloc if you do not want to
/*	{
		System.setProperty("atu.reporter.config","C:\\Backup\\JavaProjects\\AutomationFramework\\automationframework\\BigcommerceWebsiteTesting\\src\\test\\resources\\testProperties\\atu.properties");
	}*/
	
	public void runTestNGTest(Map<String,String> testngParams) {
		 
		//Create an instance on TestNG
		 TestNG myTestNG = new TestNG();
		 
		//Create an instance of XML Suite and assign a name for it.
		 XmlSuite mySuite = new XmlSuite();
		 mySuite.setName("ASQA Sample Suite");
		 
		//Create an instance of XmlTest and assign a name for it.
		 XmlTest myTest = new XmlTest(mySuite);
		 myTest.setName("ASQA Sample Test");
		 
		//Add any parameters that you want to set to the Test.
		 myTest.setParameters(testngParams);
		 
		//Create a list which can contain the classes that you want to run.
		 List<XmlClass> myClasses = new ArrayList<XmlClass> ();
		 myClasses.add(new XmlClass("com.bigcommerce.BigcommerceHomePageTest"));
		 
		//Assign that to the XmlTest Object created earlier.
		 myTest.setXmlClasses(myClasses);
		 
		//Create a list of XmlTests and add the Xmltest you created earlier to it.
		 List<XmlTest> myTests = new ArrayList<XmlTest>();
		 myTests.add(myTest);
		 
		//add the list of tests to your Suite.
		 mySuite.setTests(myTests);
		 
		//Add the suite to the list of suites.
		 List<XmlSuite> mySuites = new ArrayList<XmlSuite>();
		 mySuites.add(mySuite);
		 
		//Set the list of Suites to the testNG object you created earlier.
		 myTestNG.setXmlSuites(mySuites);
		 
		 //writes file to testPlan/testNG.xml
		 writeToFile(mySuite.toXml());
		 
		 //TODO; will remove after its stable
		 System.out.println("Printing XML " + mySuite.toXml());
		 
		//invoke run() - this will run your class.
		 myTestNG.run();
		 
		}
	
	public void writeToFile(String xmlContent){
		BufferedWriter out = null;
		try{
			out = new BufferedWriter(new FileWriter("testPlan/testNG.xml"));
			out.write(xmlContent);			
		}catch(IOException e){
			e.printStackTrace();
		}finally{
			try{
				if(out !=null) out.close();
			}catch (IOException e){
				e.printStackTrace();
				}
		}
	}
	
}
