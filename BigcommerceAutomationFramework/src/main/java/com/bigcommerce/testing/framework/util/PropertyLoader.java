package com.bigcommerce.testing.framework.util;

import java.io.IOException;
import java.util.Properties;

public class PropertyLoader {

	//private final static String PROP_FILE = "/application.properties";
	
	private final static String PROP_FILE = "C:\\Backup\\JavaProjects\\AutomationFramework\\automationframework\\BigcommerceWebsiteTesting\\src\\test\\resources\\testProperties\\application.properties";
	
	
	private final static String PROP_FILE_TEST = "test.properties";
	
	public PropertyLoader(){}
	
	public static String loadProperty(String name){
		Properties props = new Properties();
		try{
			props.load(PropertyLoader.class.getResourceAsStream(PROP_FILE));
		}catch(IOException e){
			e.printStackTrace();
		}
		
		String value = "";
		
		if(name !=null){
			value = props.getProperty(name);			
		}
		return value;
	}
	
	
	
	
}
