package com.bigcommerce.testing.framework.util;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.testng.Assert;

import atu.testng.reports.ATUReports;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

public abstract class BaseUtil {

	protected WebDriver driver;	
	final int DRIVER_WAIT = 4;
	
	public BaseUtil(WebDriver driver){
		ElementLocatorFactory finder = new 
				AjaxElementLocatorFactory(driver,DRIVER_WAIT);
		PageFactory.initElements(finder, this);
	}	

	/**
	 * Waits until page loads completely in browser
	 * @param	driver		WebDriver
	 */
	public void waitForPageToLoad(WebDriver driver){
		JavascriptExecutor js = (JavascriptExecutor) driver; 
		for(int seconds=1; seconds<=60; seconds++) {		
			try{				
				if ( js.executeScript("return document.readyState").equals("complete") ) {break;} 
			} catch(Exception e) {}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {			
				e.printStackTrace();
			}
		}
	}
	
	/*
	 * Verify for given text to appear on page
	 * If given text is not found, throws as assertion error
	 * 
	 * @param	actualText		String
	 * @param	expetedText		String	 * 
	 */
	
	public void verifyText(String actualText, String exptedText){
		try{
		Assert.assertEquals(actualText, exptedText);
		}catch(Throwable t){
			System.out.println("Error Ecountered: " + t);			
		}
	}
	
	/*
	 *  Logs information in the report
	 *  
	 *  @param  description				String	   
	 *  @param  inputValue				String
	 *  @param  expectedValue			String 
	 *  @param	actualValue				String
	 *  @param 	isScreeenShot			boolean	 *  
	 */ 
	 
	 public void logInfo(String description, String inputValue, String expectedValue, String actualValue
			 , boolean isScreenShot){
		 
		 CaptureScreen captureScreen = null;
		 if(isScreenShot == true){
			 captureScreen = new CaptureScreen(ScreenshotOf.DESKTOP);
			 ATUReports.add(description, inputValue, expectedValue, actualValue, isScreenShot);
		 }		 
	 }
	 
	 /*
	  * WebDriver will timeout in defined second 
	  * 
	  * @param 	second	int
	  * 
	  */
	 
	 public void timeOut(int second){
		 driver.manage().timeouts().pageLoadTimeout(second,TimeUnit.SECONDS);
	 }
	 
	 /*
	  * Download file from the Internet
	  * 
	  * @param  filePath	String
	  * @param	fileName	String
	  * 
	  */ 
	 public String downloadFile(String filePath, String fileName) throws InterruptedException{//begin downloadFile() method
		  	String newfileName = null;
			try{//betgin try		 
				Thread.sleep(3000);
				
				Robot robot = new Robot();
				robot.keyPress(KeyEvent.VK_SHIFT);
			    robot.keyPress(KeyEvent.VK_CONTROL);
			    robot.keyPress(KeyEvent.VK_S);			    
			    robot.keyRelease(KeyEvent.VK_SHIFT);
			    robot.keyRelease(KeyEvent.VK_CONTROL);
			    robot.keyPress(KeyEvent.VK_S);
			    
			    Thread.sleep(5000);
				
				String dateTimeStamp = getDateToday("MM-dd-yyyy-HH-mm-ss");	
				newfileName = dateTimeStamp + "_" + fileName;
				String filePathAndName = filePath + dateTimeStamp + "_" + fileName;
				
				StringSelection stringSelection = new StringSelection(filePathAndName);			
				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);				
				
				robot.keyPress(KeyEvent.VK_DELETE);
			    robot.keyRelease(KeyEvent.VK_DELETE);
			    
			    robot.keyPress(KeyEvent.VK_CONTROL);
			    robot.keyPress(KeyEvent.VK_V);
			    robot.keyRelease(KeyEvent.VK_V);
			    robot.keyRelease(KeyEvent.VK_CONTROL);
			    
			    Thread.sleep(5000);
			    
			    robot.keyPress(KeyEvent.VK_ENTER);
			    robot.keyRelease(KeyEvent.VK_ENTER);			    			    
			}//end try
			catch (Throwable e){//begin catch
				e.printStackTrace();
			}//end catch			
		  		Thread.sleep(5000);		 
			
		  	return newfileName;

		}//end of downloadFile() method

	 /*
	  * Date 
	  * 
	  * @param  dateFormat	String
	  *  
	  */ 
	 public String getDateToday(String dateFormat){
		 
		 DateFormat _dateFormat = new SimpleDateFormat(dateFormat);
		 Calendar c = Calendar.getInstance();
		 c.setTime(new Date());
		 return _dateFormat.format(c.getTime());
	 }
	 
	 /*
	  * Load properties information from the application.properties fiel
	  * 
	  * @param  fname	String
	  *  
	  *  @param  pname	String
	  */ 
	 public static String LoadENV(String fname,String pname) throws FileNotFoundException, IOException{
			Properties prop = new Properties();
			prop.load(new FileInputStream(fname));
			String pVal = prop.getProperty(pname);
			System.out.println("*** The pVal is: " + pVal);
			return pVal;
		}

}
