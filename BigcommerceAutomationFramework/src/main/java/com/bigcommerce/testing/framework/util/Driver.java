package com.bigcommerce.testing.framework.util;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

public class Driver {
		
		public static WebDriver driver = null;	
		
		//driver = new FirefoDriver();
		
		public static WebDriver browser(String browser){		
			if(browser.equalsIgnoreCase("Firefox")){		
				driver = new FirefoxDriver();
			}else if(browser.equalsIgnoreCase("IE")){
				driver = new InternetExplorerDriver();			
			}else if(browser.equalsIgnoreCase("Chrome")){
				System.setProperty("webdriver.chrome.driver", 
				   		"C:\\Backup\\ChromeDriver\\chromedriver.exe");
				driver = new ChromeDriver();
			}else if(browser.equalsIgnoreCase("Safari")){
				driver = new SafariDriver();
			}
			
			return driver;
		}
		
		public static WebDriver gridBrowser(String browser,String port) throws Exception{//begin beforeClass
			//public void beforeClass(String node_ipaddress, String browser,String port){//begin beforeClass
				    
					//WebDriver mydriver =null;		 
					DesiredCapabilities cap= new DesiredCapabilities();
					//cap.setPlatform(Platform.ANY);
					//cap= new DesiredCapabilities();
					//Genfunctionlib.cap = new DesiredCapabilities();
					//cap.setBrowserName(browser);
					//Genfunctionlib.cap.setBrowserName(browser);
					//cap.setJavascriptEnabled(true);

					//Browser
					if(browser.equalsIgnoreCase("ie"))
					cap = DesiredCapabilities.internetExplorer();
					
					if(browser.equalsIgnoreCase("Chrome")){
					System.setProperty("webdriver.chrome.driver", 
					"C:\\Backup\\ChromeDriver\\chromedriver.exe");
					cap = DesiredCapabilities.chrome();
					}
					
					if(browser.equalsIgnoreCase("Firefox"))
					cap = DesiredCapabilities.firefox();
					
					if(browser.equalsIgnoreCase("iPad"))
					cap = DesiredCapabilities.ipad();
					
					if(browser.equalsIgnoreCase("Android"))
					cap = DesiredCapabilities.android();	
					
					if(browser.equalsIgnoreCase("Safari"))
					cap = DesiredCapabilities.safari();
				  
					try { //http://www.guru99.com/introduction-to-selenium-grid.html
							//System.out.println("sleep-1");
							//Thread.sleep(10000);
							//System.out.println("sleep-1b");
						driver= new RemoteWebDriver(new URL("http://192.168.2.8:".concat(port).concat("/wd/hub")), cap);
						//Genfunctionlib.mydriver= new RemoteWebDriver(new URL("http://localhost:".concat(port).concat("/wd/hub")), cap);//this localhost is each node ipaddress not the 'hub_ip_address'. don't confuse with the dos prompt 'hub_ip_address'
						//Genfunctionlib.mydriver= new RemoteWebDriver(new URL("http://localhost:".concat(port).concat("/wd/hub")), Genfunctionlib.cap);
						//wd= new RemoteWebDriver(new URL(node_ipaddress.concat(port).concat("/wd/hub")), capability);
						
						//wd= new RemoteWebDriver(new URL("http://ip_address_of_node_machine:".concat(port).concat("/wd/hub")), capability);
						
						//mydriver.manage().window().maximize();
						//driver.get("file:///C:/Test_Html/inquiries_html/index.html");	
						//Genfunctionlib.mydriver.get("file:///C:/Test_Html/inquiries_html/index.html");	
					
					} catch (MalformedURLException e) {			    
						e.printStackTrace();		   
					}
					
						System.out.print(":: ::  Browser name is: " +  browser );
						System.out.println();
					
						return driver;
					
				 }//end of beforeClass

}
