package com.bigcommerce.testing.framework.util;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

import atu.testng.reports.ATUReports;
import atu.testng.reports.enums.ReportLabels;
import atu.testng.reports.listeners.ATUReportsListener;
import atu.testng.reports.listeners.ConfigurationListener;
import atu.testng.reports.listeners.MethodListener;
import atu.testng.reports.utils.Utils;

@Listeners({ ATUReportsListener.class, ConfigurationListener.class,
	  MethodListener.class })
public class TestConfig {
	
	//this one is working. Note: you do not have to use the static key work for static bloc if you do not want to
	{
		System.setProperty("atu.reporter.config","C:\\Backup\\JavaProjects\\AutomationFramework\\automationframework\\BigcommerceWebsiteTesting\\src\\test\\resources\\testProperties\\atu.properties");
	}
	
	//this code is also working
/*	{
		//System.setProperty("atu.reporter.config", System.getProperty("C:\\Backup\\JavaProjects\\AutomationFramework\\automationframework\\BigcommerceWebsiteTesting\\src\\test\\resources\\testProperties\\atu.properties"));
		System.setProperty("atu.reporter.config", System.getProperty("user.dir")+"\\src\\test\\resources\\testProperties\\atu.properties");
	}*/
	
	public WebDriver driver;
	private String projPath = System.getProperty("user.dir");
	private String siteUrl = null;
	private String filePath = null; //= "C:\\Backup\\JavaProjects\\AutomationFramework\\automationframework\\BigcommerceWebsiteTesting\\src\\test\\resources\\testProperties\\application.properties";
	
/*	@BeforeSuite
	public void setupBeforeSuite(){
		//String logpath = System.getProperty("user.dir");		
		siteUrl = PropertyLoader.loadProperty("site.url");
	}*/
	
	@BeforeSuite
	public void setupBeforeSuite() throws FileNotFoundException, IOException{		
		filePath = projPath + "/src/test/resources/testProperties/application.properties";
		siteUrl = BaseUtil.LoadENV(filePath, "site.url");		
	}	
	
	@BeforeClass(alwaysRun=true)
    public void init() {
			
		//System.setProperty("webdriver.gecko.driver", "C:\\Selenium-java-3.0.1\\geckodriver.exe");
		//stem.setProperty("webdriver.chrome.driver", "C:\\Selenium-java-3.0.1\\chromedriver.exe");
		
/*		System.setProperty("webdriver.gecko.driver", 
		"C:\\Backup\\JavaProjects\\AutomationFramework\\automationframework"
		+ "\\BigcommerceWebsiteTesting\\src\\test\\resources\\Driver\\geckodriver\\geckodriver.exe");*/
		
		 System.setProperty("webdriver.chrome.driver", "C:\\Backup\\JavaProjects\\AutomationFramework"
		 		+ "\\automationframework\\BigcommerceWebsiteTesting\\src\\test\\resources\\Driver"
		 		+ "\\chromedriver\\chromedriver.exe");
         
		 //driver = new FirefoxDriver();		
         driver = new ChromeDriver();
        //driver = new InternetExplorerDriver();
         ATUReports.setWebDriver(driver);
         setIndexPageDescription();
         ReportLabels.ATU_CAPTION.setLabel("BIGCOMMERCE");
         //setAuthorInfoForReports("Khan M Rahman");//it does not work here. you have to put in the @Test testcase method
         driver.manage().window().maximize();
         System.out.println("Before getting the url");
         //driver.get("http://www.bigcommerce.com");
         driver.get(siteUrl);
         System.out.println("After getting the url");          
    }
	
	public void setIndexPageDescription(){
		ATUReports.indexPageDescription = "<b><font size='5' color='blue'><em>Bigcommerce Automation Report</em> </font></b></br></br></br>"
				+ "</br></br></br></br>Yet to add project description and content on this page</br>For now please click on the "
				+ "<b>'Consolidated Reports'</b> to view the recent reports</br></br></br></br><b><em>Thanks....</em></b>";
	}
	
	public void setAuthorInfoForReports(String authorName){
		ATUReports.setAuthorInfo(authorName, Utils.getCurrentTime(), "1.0");
	}
	
	@AfterClass(alwaysRun=true)
	public void closeBrowser(){
		driver.close();
		System.out.println("Browser closed");		
	}

}
