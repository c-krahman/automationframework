package com.bigcommerce;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;

import com.bigcommerce.po.enterprise.EnterpriceCommerceSolutionPO;
import com.bigcommerce.po.homepage.BigcommerceHomePagePO;
import com.bigcommerce.testing.framework.util.BaseUtil;
import com.bigcommerce.testing.framework.util.Driver;
import com.bigcommerce.testing.framework.util.TestConfig;

import atu.testng.reports.listeners.ATUReportsListener;
import atu.testng.reports.listeners.ConfigurationListener;
import atu.testng.reports.listeners.MethodListener;

//*****Using the Test_Run.xml it is not working properly for screenshot and not reading from the atu.properties file. 
//*****TestConfig.java is working fine:
/*@Listeners({ ATUReportsListener.class, ConfigurationListener.class,
	  MethodListener.class })*/
public class BigcommerceHomePageTest extends TestConfig{
	//this one is working. Note: you do not have to use the static key work for static bloc if you do not want to
/*	{
		System.setProperty("atu.reporter.config", 
		"C:\\Backup\\JavaProjects\\AutomationFramework\\automationframework\\BigcommerceWebsiteTesting\\src\\test\\resources\\testProperties\\atu.properties");
	}*/ 
	
	//this one is also working
/*	{
		//System.setProperty("atu.reporter.config", System.getProperty("C:\\Backup\\JavaProjects\\AutomationFramework\\automationframework\\BigcommerceWebsiteTesting\\src\\test\\resources\\testProperties\\atu.properties"));
		System.setProperty("atu.reporter.config", System.getProperty("user.dir")+"\\src\\test\\resources\\testProperties\\atu.properties");
	}*/

	//public WebDriver driver;
	
	//****Using the TestConfig.java file and it is working. So, no need to use the @BeforeClass here
/*	@BeforeClass
    public void init() {
		//System.setProperty("webdriver.gecko.driver", "C:\\Selenium-java-3.0.1\\geckodriver.exe");
		//stem.setProperty("webdriver.chrome.driver", "C:\\Selenium-java-3.0.1\\chromedriver.exe");
		
		System.setProperty("webdriver.gecko.driver", 
		"C:\\Backup\\JavaProjects\\AutomationFramework\\automationframework"
		+ "\\BigcommerceWebsiteTesting\\src\\test\\resources\\Driver\\geckodriver\\geckodriver.exe");
		
		 System.setProperty("webdriver.chrome.driver", "C:\\Backup\\JavaProjects\\AutomationFramework"
		 		+ "\\automationframework\\BigcommerceWebsiteTesting\\src\\test\\resources\\Driver"
		 		+ "\\chromedriver\\chromedriver.exe");
         
		 //driver = new FirefoxDriver();		
         driver = new ChromeDriver();
        //driver = new InternetExplorerDriver();
          ATUReports.setWebDriver(driver);
          driver.manage().window().maximize();
    }*/	
	
	@Test(enabled=true, priority=0)
	public void HomePageTest() throws InterruptedException{						
		System.out.println("This is a test");
		setAuthorInfoForReports("Khan M Rahman");
		//driver.get("http://www.bigcommerce.com");	
		//Thread.sleep(5000);
		
		BigcommerceHomePagePO homePage = PageFactory.initElements(driver, BigcommerceHomePagePO.class);		
		System.out.println("Before clickEnterprise method");				
		//EnterpriceCommerceSolutionPO entrprssol = homePage.clickEnterPrseLnk(driver);
		System.out.println("After clicking enterprise method");
		//Thread.sleep(10000);
		System.out.println("The current url is from the test case: " + driver.getCurrentUrl());
		//entrprssol.clikBigcomloglnk(driver);
	}
	
	//****Using the TestConfig.java file and it is working. so need for the @AfterClass here:
/*	@AfterClass
	public void closeBrowser(){
		driver.close();
		System.out.println("Browser closed");
		
	}*/

}
