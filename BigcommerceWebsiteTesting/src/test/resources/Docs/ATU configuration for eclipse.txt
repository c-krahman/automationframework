Download and configure ATU from: 
http://automationtestingutilities.blogspot.com/p/reporting.html
http://automationtestingutilities.blogspot.in/2013/12/ATUReporterPart2.html

Another useful site: http://gururajhm.blogspot.com/p/blog-page_68.html

==========================

Configure ATU dependency in pom:

Note: If you add the ATU jars to the build path in Eclipse, you will not be able to compile or test using maven and will get surefire error and other errors...

**** So, Make sure to configure ATU jars dependency as follows in Eclips and pom.xml

1. File->Import->Install or deploy an artifact to a Maven repository
2. Next
3. Artifact file: browse to the folder where you have the ATU reporter jar	ATUReporter_Selenium_testNG_5.5_BETA
4. POM file: browse to the folder where you have the ATU recorder jar ATUTestRecorder_2.1
5. Group Id: type something. For example: automation.test.framework
6. Artifact Id: type something. For example: automation_reporter
7. In Version type 5.5 for reporter and 2.1 for recorder
8. In Packaging select 'jar' from the drop donw list
9. Click Finish

Now add the dependency tag in the pom file as follows:

  	    <dependency>
		   	<groupId>automation.test.framework</groupId>
		   	<artifactId>automation_reporter</artifactId>
		   	<version>5.5</version>
	    </dependency>
	    
	    <dependency>
		   	<groupId>automation.test.framework</groupId>
		   	<artifactId>automation_recorder</artifactId>
		   	<version>2.1</version>
	    </dependency>
	    
===================

For video recorder to work in Firefox browser:

1. Download vlc palyer: http://www.videolan.org/vlc/download-windows.html
2. Install 
3. Refresh browser

Note: Without installing the VLC player, you are not going to able to view any video

===================

 