package com.bigcommerce.po.enterprise;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.bigcommerce.po.homepage.BigcommerceHomePagePO;
import com.bigcommerce.testing.framework.util.BaseUtil;

public class EnterpriceCommerceSolutionPO extends BaseUtil{
	
	public static String ACTUAL_HOME_URL; 
	public static String EXPECTED_HOME_URL = "https://www.bigcommerce.com/";

	public EnterpriceCommerceSolutionPO(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(xpath = ".//*[@id='mm-0']/header/div/nav/ul/li[1]/a/img[2]")	
	public WebElement lnkLogoBigcmrc;
	
	public BigcommerceHomePagePO clikBigcomloglnk(WebDriver driver) throws InterruptedException{
		waitForPageToLoad(driver);
		System.out.println("Before clicking Bigcommerce logo link");
		lnkLogoBigcmrc.click();
		System.out.println("After clicking Bigcommerce logo link");		
		waitForPageToLoad(driver);
		Thread.sleep(5000);	
		ACTUAL_HOME_URL = driver.getCurrentUrl();
		System.out.println("Back to home page and title is: " + driver.getCurrentUrl());
		logInfo("Enterprise Page", "Click Bigcommerce logo link", ACTUAL_HOME_URL, EXPECTED_HOME_URL, true);
		return PageFactory.initElements(driver, BigcommerceHomePagePO.class);
	}

}
