package com.bigcommerce.po.homepage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.bigcommerce.po.enterprise.EnterpriceCommerceSolutionPO;
import com.bigcommerce.testing.framework.util.BaseUtil;
import com.bigcommerce.testing.framework.util.Driver;

public class BigcommerceHomePagePO extends BaseUtil{

	public BigcommerceHomePagePO(WebDriver driver) {
		super(driver);		
	}	
	
	public static String EXPECTED_TITLE = "1Ecommerce Software & Shopping Cart Platform | BigCommerce";
	public static String ACTUAL_TITLE;
	public static String EXPECTED_ENTPRISE_URL = "https://www.bigcommerce.com/enterprise-ecommerce-solution/";
	public static String ACTUAL_ENTPRISE_URL;
	
	@FindBy(xpath = ".//*[@id='mm-0']/header/div/nav/section/ul/li[1]/a")	
	public WebElement entrprseLnk;
	
	public EnterpriceCommerceSolutionPO clickEnterPrseLnk(WebDriver driver) throws InterruptedException{
		waitForPageToLoad(driver);
		ACTUAL_TITLE = driver.getTitle();
		verifyText(ACTUAL_TITLE, EXPECTED_TITLE);
		System.out.println("After verifyText method");
		logInfo("Open browser", "Type www.bigcommerce.com", ACTUAL_TITLE, EXPECTED_TITLE, true);
		System.out.println("After logInfo method");
		entrprseLnk.click();		
		System.out.println("***Enter Prise link clicked");
		waitForPageToLoad(driver);
		Thread.sleep(5000);
		ACTUAL_ENTPRISE_URL = driver.getCurrentUrl();
		System.out.println("Passed thread sleep");		
		logInfo("Bigcommerce Home Page", "Click Enterprise link", ACTUAL_ENTPRISE_URL, EXPECTED_ENTPRISE_URL, true);
		System.out.println("The current url is inside click enterprise link method: " + driver.getCurrentUrl());
		driver.navigate().refresh();
		waitForPageToLoad(driver);
		return PageFactory.initElements(driver, EnterpriceCommerceSolutionPO.class);
	}

}
